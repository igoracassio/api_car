# Api de cadastro de carros #

O repositório contém código java utilizando arquitetura rest e o ecosistema do framework spring

### Qual o propósito dessa api? ###

 Realizar o cadastro de carros uma vez que alguns dados do mesmo devem ser consultados utilizando a api da fipe.
 link da api da fipe para consulta e análise do teste: http://fipeapi.appspot.com/

### Como configurar ? ###

  Sumário de configurações:
  
  O banco utilizado é o postgres, portanto é necessário te-lo instalado na máquina. 
  As dependências do projeto utilizam o gerenciador maven e estão adicionadas no arquivo pom.xml 
  As configurações de acesso ao banco de dados estão no arquivo application.properties também 
  contida na aplicação. No entanto é necessário criar o banco de dados com o nome "cars"
  
  Segue as configurações :
  	
 spring.datasource.url=jdbc:postgresql://localhost:5432/cars
 spring.datasource.username=postgres
 spring.datasource.password=1234
 
 obs: O usuário utilizado é o default do postgres e o password configurado na minha máquina é esse.
 O password pode ser alterado desde que que no arquivo spring.datasource.password também esteja setado
 com o mesmo password.
	

### Informações para teste da applicação ###

 Para utilização da api é necessário ter um client http, no meu caso eu utilizei o postman
 para realizar as requests da api. 
 
 Como para o consumo dessa api foi utilizado JSON para a requisição segue o 
 formato de JSON para a request do verbo Post para cadastro do veículo: 
 
 a URL da requisição é "localhost:8080/api/v1/cars"
 
 {
    "placa": "XYZ-1234"
    "idMarca":"21",
    "idModelo":"4828",
    "precoAnuncio": "1234.56, ",
    "ano":"2013-1"
}

para a requisição de consulta segue a mesma URL porém lembrando de setar o tipo Get
da requisição. 


### Para dúvidas segue meu contato ###

 email: igormelo9001@gmail.com 
 ou whatsapp
 34 988506313