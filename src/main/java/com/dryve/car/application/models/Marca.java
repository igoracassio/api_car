package com.dryve.car.application.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "marca")
public class Marca {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column
    private String fipe_id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFipe_id() {
        return fipe_id;
    }

    public void setFipe_id(String fipe_id) {
        this.fipe_id = fipe_id;
    }
}
