package com.dryve.car.application.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "placa")
    private String placa;

    @Column(name = "id_marca")
    private String idMarca;

    @Column(name = "id_modelo")
    private String idModelo;

    @Column(name = "preco_anuncio")
    private String precoAnuncio;

    @Column(name = "preco_fipe")
    private String precoFipe;

    @Column(name = "ano")
    private String ano;

    @Column(name = "data_cadastro")
    private Date data;

    public String getPreco_fipe() {
        return precoFipe;
    }

    public void setPreco_fipe(String precoFipe) {
        this.precoFipe = precoFipe;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Car(){

    }

    public Car(long id, String placa, String idMarca, String idModelo, String precoAnuncio, String ano) {
        this.id = id;
        this.placa = placa;
        this.idMarca = idMarca;
        this.idModelo = idModelo;
        this.precoAnuncio = precoAnuncio;
        this.ano = ano;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(String idMarca) {
        this.idMarca = idMarca;
    }

    public String getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(String idModelo) {
        this.idModelo = idModelo;
    }

    public String getPrecoAnuncio() {
        return precoAnuncio;
    }

    public void setPrecoAnuncio(String precoAnuncio) {
        this.precoAnuncio = precoAnuncio;
    }

    public String getAno(String ano) {
        return this.ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getAno() {
        return this.ano;
    }
}
