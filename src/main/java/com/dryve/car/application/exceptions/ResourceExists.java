package com.dryve.car.application.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class ResourceExists extends Exception{

    private static final long serialVersionUID = 1L;
    private final String message;

    public ResourceExists(String message) throws Exception{
        super(message);
        this.message = message;
    }
}
