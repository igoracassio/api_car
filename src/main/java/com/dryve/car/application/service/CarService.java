package com.dryve.car.application.service;

import com.dryve.car.application.models.Car;

import java.util.List;
import java.util.Optional;

public interface CarService {

    Car save(Car car);

    Car update(Car car);

    List<Car> findAll();

    Optional<Car> findById(Long id);

    void delete(Car car);

    Optional<Car> findByPlaca(String placa);

}
