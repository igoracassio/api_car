package com.dryve.car.application.controllers;

import com.dryve.car.application.converters.CarConverter;
import com.dryve.car.application.dtos.CarDTO;
import com.dryve.car.application.exceptions.ResourceExists;
import com.dryve.car.application.exceptions.ResourceNotFountException;
import com.dryve.car.application.models.Car;
import com.dryve.car.application.models.Response;
import com.dryve.car.application.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/v1")
public class CarController {

    @Autowired
    private CarService carService;

    @Autowired
    private CarConverter carConverter;

    @Autowired
    private WebClient webClient;

    @GetMapping("/cars")
    public List<CarDTO> getAllCars(){

        List<Car> carList = this.carService.findAll();
        List<CarDTO> dtoList = this.carConverter.transformEntityListToDtoList(carList);

        return dtoList;

    }

    @GetMapping("/cars/{id}")
    public ResponseEntity<CarDTO> getCarById(@PathVariable(value = "id") Long id)
            throws ResourceNotFountException {
        Optional<Car> car = Optional.ofNullable(carService.findById(id)
                .orElseThrow(() -> new ResourceNotFountException("Carro não foi encontrado com id :: " + id)));
        CarDTO carDTO = this.carConverter.transformEntityToDTO(car.get());
                return ResponseEntity.ok(carDTO);
    }

    @PostMapping("/cars")
    public ResponseEntity<CarDTO> createCar(@Valid @RequestBody CarDTO carDTO) throws Exception {
        String url = "http://fipeapi.appspot.com/api/1/carros/veiculo/" + carDTO.getIdMarca()
                + "/" + carDTO.getIdModelo() + "/" + carDTO.getAno() + ".json";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        headers.set("X-Request-Source", "Desktop");

        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Response> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                Response.class
        );

        Response res = (Objects.requireNonNull(response.getBody()));

        Car car = new Car();
        car = this.carConverter.transformDtoToEntity(carDTO);
        car.setAno(res.getAno_modelo());
        car.setIdMarca(res.getMarca());
        car.setIdModelo(res.getVeiculo());
        car.setPreco_fipe(res.getPreco());
        car.setPrecoAnuncio(carDTO.getPrecoAnuncio());
        car.setPlaca(carDTO.getPlaca());
        car.setData(new Date());

        Optional<Car> opt = this.carService.findByPlaca(car.getPlaca());

        if(opt.isPresent()){
            throw new ResourceExists("Carro com a placa " + car.getPlaca() + "já cadastrada");
        }
        car = this.carService.save(car);
        CarDTO dto = this.carConverter.transformEntityToDTO(car);
        dto.setPrecoFipe(car.getPreco_fipe());

        return ResponseEntity.ok(dto);
    }

    @PutMapping("/cars/{id}")
    public ResponseEntity<CarDTO> updateCar(@PathVariable(value = "id") Long id,
          @Valid @RequestBody CarDTO carDTO) throws ResourceNotFountException {
        Car car = this.carConverter.transformDtoToEntity(carDTO);
        car = this.carService.findById(id)
                .orElseThrow(() -> new ResourceNotFountException("Carro não foi encontrado com o id :: " + id));
        car.setAno(carDTO.getAno());
        car.setId(carDTO.getId());
        car.setIdMarca(carDTO.getIdMarca());
        car.setIdModelo(carDTO.getIdModelo());
        car.setPrecoAnuncio(carDTO.getPrecoAnuncio());

        final Car updatedCar = this.carService.update(car);
        CarDTO dto = this.carConverter.transformEntityToDTO(updatedCar);
        return ResponseEntity.ok(dto);
    }

    @DeleteMapping("/cars/{id}")
    public Map<String, Boolean> deleteCar(@PathVariable(value = "id") Long id) throws ResourceNotFountException {
        Car car = this.carService.findById(id)
                .orElseThrow(() -> new ResourceNotFountException("Carro não foi encontrado com o id :: " + id));

        this.carService.delete(car);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);

        return response;
    }
}
