package com.dryve.car.application.serviceImpl;

import com.dryve.car.application.models.Car;
import com.dryve.car.application.repositories.CarRepository;
import com.dryve.car.application.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.Optional;

@Repository
public class CarServiceImpl implements CarService {
    
    @Autowired
    private CarRepository carRepository;

    @Autowired
    private WebClient webClient;
    
    @Override
    public Car save(Car car) {
        return this.carRepository.save(car);
    }

    @Override
    public Car update(Car car) {
        return this.carRepository.save(car);
    }

    @Override
    public List<Car> findAll() {
        return this.carRepository.findAll();
    }

    @Override
    public Optional<Car> findById(Long id) {
        return this.carRepository.findById(id);
    }

    @Override
    public void delete(Car car) {
        this.carRepository.delete(car);
    }

    @Override
    public Optional<Car> findByPlaca(String placa) {
        return this.carRepository.findByPlaca(placa);
    }

}
