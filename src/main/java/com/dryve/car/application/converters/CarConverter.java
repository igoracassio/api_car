package com.dryve.car.application.converters;

import com.dryve.car.application.dtos.CarDTO;
import com.dryve.car.application.models.Car;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CarConverter extends ModelMapper{

    @Autowired
    private ModelMapper modelMapper;

    public Car transformDtoToEntity(CarDTO carDTO){
        Car car = new Car();
        car = this.modelMapper.map(carDTO, Car.class);
        return car;
    }

    public CarDTO transformEntityToDTO(Car car){
        CarDTO carDTO = new CarDTO();
        carDTO = this.modelMapper.map(car, CarDTO.class);
        return carDTO;
    }

    public List<Car> transformDtoListToEntityList(List<CarDTO> carDTOSList){
        return carDTOSList.stream().map(carDTO -> transformDtoToEntity(carDTO)).collect(Collectors.toList());
    }

    public List<CarDTO> transformEntityListToDtoList(List<Car> carList){
        return carList.stream().map(car -> transformEntityToDTO(car)).collect(Collectors.toList());

    }
}
