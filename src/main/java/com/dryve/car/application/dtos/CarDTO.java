package com.dryve.car.application.dtos;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import java.util.Date;

@Component
public class CarDTO {

    private long id;

    private String placa;

    private String idMarca;

    private String idModelo;

    private String precoAnuncio;

    private String ano;

    private String precoFipe;

    private Date data;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(String idMarca) {
        this.idMarca = idMarca;
    }

    public String getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(String idModelo) {
        this.idModelo = idModelo;
    }

    public String getPrecoAnuncio() {
        return precoAnuncio;
    }

    public void setPrecoAnuncio(String precoAnuncio) {
        this.precoAnuncio = precoAnuncio;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getPrecoFipe() {
        return precoFipe;
    }

    public void setPrecoFipe(String precoFipe) {
        this.precoFipe = precoFipe;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
